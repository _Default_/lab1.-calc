public class Test {
    public  static void main (String[] args)
    {
        test(Calc.add(2,3), 5);
        test(Calc.minus(3,2), 1);
        test(Calc.inc(2,3),6);
        test(Calc.dec(8,2),4);
        test(Calc.add(100000,20), 100000);
        test(Calc.minus(32600,200), 32400);
        test(Calc.inc(1000,1000),1000000);
        test(Calc.dec(1000000,2),500001);
        test(Calc.add(-100000,-20), -100020);
        test(Calc.minus(-32600,200), -32400);
        test(Calc.inc(1000,-1000),-1000000);
        test(Calc.dec(-1000000,2),-500000);
    }

    public static void test(int actual, int expected)
    {
        if (actual==expected)
        {
            System.out.println("Ok");
        }
        else
        {
            System.err.println("FAILED: expected " +  expected + " but found " + actual);
        }
    }
}
